﻿namespace Fourth_Version_Of_The_Calculator
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Normal_Calculator = new System.Windows.Forms.ToolStripMenuItem();
            this.Engineering_Calculator = new System.Windows.Forms.ToolStripMenuItem();
            this.Temperature = new System.Windows.Forms.ToolStripMenuItem();
            this.Multiplication1 = new System.Windows.Forms.ToolStripMenuItem();
            this.Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.delete = new System.Windows.Forms.Button();
            this.clear = new System.Windows.Forms.Button();
            this.clear_all = new System.Windows.Forms.Button();
            this.plus = new System.Windows.Forms.Button();
            this.minus = new System.Windows.Forms.Button();
            this.nine = new System.Windows.Forms.Button();
            this.eight = new System.Windows.Forms.Button();
            this.seven = new System.Windows.Forms.Button();
            this.multiplication = new System.Windows.Forms.Button();
            this.six = new System.Windows.Forms.Button();
            this.five = new System.Windows.Forms.Button();
            this.four = new System.Windows.Forms.Button();
            this.division = new System.Windows.Forms.Button();
            this.three = new System.Windows.Forms.Button();
            this.two = new System.Windows.Forms.Button();
            this.one = new System.Windows.Forms.Button();
            this.equals = new System.Windows.Forms.Button();
            this.comma = new System.Windows.Forms.Button();
            this.zero = new System.Windows.Forms.Button();
            this.minus_plus = new System.Windows.Forms.Button();
            this.Pi = new System.Windows.Forms.Button();
            this.log = new System.Windows.Forms.Button();
            this.sqrt = new System.Windows.Forms.Button();
            this.x2 = new System.Windows.Forms.Button();
            this.x3 = new System.Windows.Forms.Button();
            this.dec = new System.Windows.Forms.Button();
            this.sin = new System.Windows.Forms.Button();
            this.sinh = new System.Windows.Forms.Button();
            this.x1 = new System.Windows.Forms.Button();
            this.bin = new System.Windows.Forms.Button();
            this.cos = new System.Windows.Forms.Button();
            this.cosh = new System.Windows.Forms.Button();
            this.lnx = new System.Windows.Forms.Button();
            this.hex = new System.Windows.Forms.Button();
            this.tan = new System.Windows.Forms.Button();
            this.tanh = new System.Windows.Forms.Button();
            this.pros = new System.Windows.Forms.Button();
            this.oct = new System.Windows.Forms.Button();
            this.mod = new System.Windows.Forms.Button();
            this.exp = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button42 = new System.Windows.Forms.Button();
            this.button41 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Fahrenheit_To_Celsius = new System.Windows.Forms.RadioButton();
            this.Вegrees_Kevin = new System.Windows.Forms.RadioButton();
            this.Celsius_To_Fahrenheit = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.button44 = new System.Windows.Forms.Button();
            this.button43 = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(17)))), ((int)(((byte)(35)))));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.Exit});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(937, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Normal_Calculator,
            this.Engineering_Calculator,
            this.Temperature,
            this.Multiplication1});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.файлToolStripMenuItem.Text = "Вид";
            // 
            // Normal_Calculator
            // 
            this.Normal_Calculator.BackColor = System.Drawing.Color.Red;
            this.Normal_Calculator.Name = "Normal_Calculator";
            this.Normal_Calculator.Size = new System.Drawing.Size(180, 22);
            this.Normal_Calculator.Text = "Обычный";
            this.Normal_Calculator.Click += new System.EventHandler(this.ОбычныйToolStripMenuItem_Click);
            // 
            // Engineering_Calculator
            // 
            this.Engineering_Calculator.BackColor = System.Drawing.Color.Red;
            this.Engineering_Calculator.Name = "Engineering_Calculator";
            this.Engineering_Calculator.Size = new System.Drawing.Size(180, 22);
            this.Engineering_Calculator.Text = "Инженерный";
            this.Engineering_Calculator.Click += new System.EventHandler(this.ИнToolStripMenuItem_Click);
            // 
            // Temperature
            // 
            this.Temperature.BackColor = System.Drawing.Color.Red;
            this.Temperature.Name = "Temperature";
            this.Temperature.Size = new System.Drawing.Size(180, 22);
            this.Temperature.Text = "Температура";
            this.Temperature.Click += new System.EventHandler(this.ТемператураToolStripMenuItem_Click);
            // 
            // Multiplication1
            // 
            this.Multiplication1.BackColor = System.Drawing.Color.Red;
            this.Multiplication1.Name = "Multiplication1";
            this.Multiplication1.Size = new System.Drawing.Size(180, 22);
            this.Multiplication1.Text = "Умножение";
            this.Multiplication1.Click += new System.EventHandler(this.ТаблицаУмноженияToolStripMenuItem_Click);
            // 
            // Exit
            // 
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(53, 20);
            this.Exit.Text = "Выход";
            this.Exit.Click += new System.EventHandler(this.ExitToolStripMenuItem1_Click);
            // 
            // delete
            // 
            this.delete.BackColor = System.Drawing.Color.DarkRed;
            this.delete.FlatAppearance.BorderSize = 0;
            this.delete.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Brown;
            this.delete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.delete.Location = new System.Drawing.Point(0, 0);
            this.delete.Name = "delete";
            this.delete.Size = new System.Drawing.Size(50, 50);
            this.delete.TabIndex = 2;
            this.delete.Text = "DEL";
            this.delete.UseVisualStyleBackColor = false;
            this.delete.Click += new System.EventHandler(this.Button1_Click);
            // 
            // clear
            // 
            this.clear.BackColor = System.Drawing.Color.DarkRed;
            this.clear.FlatAppearance.BorderSize = 0;
            this.clear.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Brown;
            this.clear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.clear.Location = new System.Drawing.Point(50, 0);
            this.clear.Name = "clear";
            this.clear.Size = new System.Drawing.Size(50, 50);
            this.clear.TabIndex = 3;
            this.clear.Text = "CE";
            this.clear.UseVisualStyleBackColor = false;
            this.clear.Click += new System.EventHandler(this.Button2_Click);
            // 
            // clear_all
            // 
            this.clear_all.BackColor = System.Drawing.Color.DarkRed;
            this.clear_all.FlatAppearance.BorderSize = 0;
            this.clear_all.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Brown;
            this.clear_all.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.clear_all.Location = new System.Drawing.Point(100, 0);
            this.clear_all.Name = "clear_all";
            this.clear_all.Size = new System.Drawing.Size(50, 50);
            this.clear_all.TabIndex = 4;
            this.clear_all.Text = "C";
            this.clear_all.UseVisualStyleBackColor = false;
            this.clear_all.Click += new System.EventHandler(this.Button3_Click);
            // 
            // plus
            // 
            this.plus.BackColor = System.Drawing.Color.DarkRed;
            this.plus.FlatAppearance.BorderSize = 0;
            this.plus.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Coral;
            this.plus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.plus.Location = new System.Drawing.Point(150, 0);
            this.plus.Name = "plus";
            this.plus.Size = new System.Drawing.Size(50, 50);
            this.plus.TabIndex = 5;
            this.plus.Text = "+";
            this.plus.UseVisualStyleBackColor = false;
            this.plus.Click += new System.EventHandler(this.Button_Aritmetic);
            // 
            // minus
            // 
            this.minus.FlatAppearance.BorderSize = 0;
            this.minus.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Coral;
            this.minus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.minus.Location = new System.Drawing.Point(150, 50);
            this.minus.Name = "minus";
            this.minus.Size = new System.Drawing.Size(50, 50);
            this.minus.TabIndex = 9;
            this.minus.Text = "-";
            this.minus.UseVisualStyleBackColor = true;
            this.minus.Click += new System.EventHandler(this.Button_Aritmetic);
            // 
            // nine
            // 
            this.nine.FlatAppearance.BorderSize = 0;
            this.nine.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Brown;
            this.nine.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.nine.Location = new System.Drawing.Point(100, 50);
            this.nine.Name = "nine";
            this.nine.Size = new System.Drawing.Size(50, 50);
            this.nine.TabIndex = 8;
            this.nine.Text = "9";
            this.nine.UseVisualStyleBackColor = true;
            this.nine.Click += new System.EventHandler(this.Button_Click);
            // 
            // eight
            // 
            this.eight.FlatAppearance.BorderSize = 0;
            this.eight.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Brown;
            this.eight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.eight.Location = new System.Drawing.Point(50, 50);
            this.eight.Name = "eight";
            this.eight.Size = new System.Drawing.Size(50, 50);
            this.eight.TabIndex = 7;
            this.eight.Text = "8";
            this.eight.UseVisualStyleBackColor = true;
            this.eight.Click += new System.EventHandler(this.Button_Click);
            // 
            // seven
            // 
            this.seven.FlatAppearance.BorderSize = 0;
            this.seven.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Brown;
            this.seven.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.seven.Location = new System.Drawing.Point(0, 50);
            this.seven.Name = "seven";
            this.seven.Size = new System.Drawing.Size(50, 50);
            this.seven.TabIndex = 6;
            this.seven.Text = "7";
            this.seven.UseVisualStyleBackColor = true;
            this.seven.Click += new System.EventHandler(this.Button_Click);
            // 
            // multiplication
            // 
            this.multiplication.FlatAppearance.BorderSize = 0;
            this.multiplication.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Coral;
            this.multiplication.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.multiplication.Location = new System.Drawing.Point(150, 100);
            this.multiplication.Name = "multiplication";
            this.multiplication.Size = new System.Drawing.Size(50, 50);
            this.multiplication.TabIndex = 13;
            this.multiplication.Text = "*";
            this.multiplication.UseVisualStyleBackColor = true;
            this.multiplication.Click += new System.EventHandler(this.Button_Aritmetic);
            // 
            // six
            // 
            this.six.FlatAppearance.BorderSize = 0;
            this.six.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Brown;
            this.six.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.six.Location = new System.Drawing.Point(100, 100);
            this.six.Name = "six";
            this.six.Size = new System.Drawing.Size(50, 50);
            this.six.TabIndex = 12;
            this.six.Text = "6";
            this.six.UseVisualStyleBackColor = true;
            this.six.Click += new System.EventHandler(this.Button_Click);
            // 
            // five
            // 
            this.five.FlatAppearance.BorderSize = 0;
            this.five.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Brown;
            this.five.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.five.Location = new System.Drawing.Point(50, 100);
            this.five.Name = "five";
            this.five.Size = new System.Drawing.Size(50, 50);
            this.five.TabIndex = 11;
            this.five.Text = "5";
            this.five.UseVisualStyleBackColor = true;
            this.five.Click += new System.EventHandler(this.Button_Click);
            // 
            // four
            // 
            this.four.FlatAppearance.BorderSize = 0;
            this.four.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Brown;
            this.four.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.four.Location = new System.Drawing.Point(0, 100);
            this.four.Name = "four";
            this.four.Size = new System.Drawing.Size(50, 50);
            this.four.TabIndex = 10;
            this.four.Text = "4";
            this.four.UseVisualStyleBackColor = true;
            this.four.Click += new System.EventHandler(this.Button_Click);
            // 
            // division
            // 
            this.division.FlatAppearance.BorderSize = 0;
            this.division.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Coral;
            this.division.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.division.Location = new System.Drawing.Point(150, 150);
            this.division.Name = "division";
            this.division.Size = new System.Drawing.Size(50, 50);
            this.division.TabIndex = 17;
            this.division.Text = "/";
            this.division.UseVisualStyleBackColor = true;
            this.division.Click += new System.EventHandler(this.Button_Aritmetic);
            // 
            // three
            // 
            this.three.FlatAppearance.BorderSize = 0;
            this.three.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Brown;
            this.three.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.three.Location = new System.Drawing.Point(100, 150);
            this.three.Name = "three";
            this.three.Size = new System.Drawing.Size(50, 50);
            this.three.TabIndex = 16;
            this.three.Text = "3";
            this.three.UseVisualStyleBackColor = true;
            this.three.Click += new System.EventHandler(this.Button_Click);
            // 
            // two
            // 
            this.two.FlatAppearance.BorderSize = 0;
            this.two.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Brown;
            this.two.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.two.Location = new System.Drawing.Point(50, 150);
            this.two.Name = "two";
            this.two.Size = new System.Drawing.Size(50, 50);
            this.two.TabIndex = 15;
            this.two.Text = "2";
            this.two.UseVisualStyleBackColor = true;
            this.two.Click += new System.EventHandler(this.Button_Click);
            // 
            // one
            // 
            this.one.FlatAppearance.BorderSize = 0;
            this.one.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Brown;
            this.one.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.one.Location = new System.Drawing.Point(0, 150);
            this.one.Name = "one";
            this.one.Size = new System.Drawing.Size(50, 50);
            this.one.TabIndex = 14;
            this.one.Text = "1";
            this.one.UseVisualStyleBackColor = true;
            this.one.Click += new System.EventHandler(this.Button_Click);
            // 
            // equals
            // 
            this.equals.FlatAppearance.BorderSize = 0;
            this.equals.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.equals.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.equals.Location = new System.Drawing.Point(150, 200);
            this.equals.Name = "equals";
            this.equals.Size = new System.Drawing.Size(50, 50);
            this.equals.TabIndex = 21;
            this.equals.Text = "=";
            this.equals.UseVisualStyleBackColor = true;
            this.equals.Click += new System.EventHandler(this.Button17_Click);
            // 
            // comma
            // 
            this.comma.FlatAppearance.BorderSize = 0;
            this.comma.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Coral;
            this.comma.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comma.Location = new System.Drawing.Point(100, 200);
            this.comma.Name = "comma";
            this.comma.Size = new System.Drawing.Size(50, 50);
            this.comma.TabIndex = 20;
            this.comma.Text = ".";
            this.comma.UseVisualStyleBackColor = true;
            this.comma.Click += new System.EventHandler(this.Button_Click);
            // 
            // zero
            // 
            this.zero.FlatAppearance.BorderSize = 0;
            this.zero.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Brown;
            this.zero.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.zero.Location = new System.Drawing.Point(50, 200);
            this.zero.Name = "zero";
            this.zero.Size = new System.Drawing.Size(50, 50);
            this.zero.TabIndex = 19;
            this.zero.Text = "0";
            this.zero.UseVisualStyleBackColor = true;
            this.zero.Click += new System.EventHandler(this.Button_Click);
            // 
            // minus_plus
            // 
            this.minus_plus.FlatAppearance.BorderSize = 0;
            this.minus_plus.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Coral;
            this.minus_plus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.minus_plus.Location = new System.Drawing.Point(0, 200);
            this.minus_plus.Name = "minus_plus";
            this.minus_plus.Size = new System.Drawing.Size(50, 50);
            this.minus_plus.TabIndex = 18;
            this.minus_plus.Text = "±";
            this.minus_plus.UseVisualStyleBackColor = true;
            this.minus_plus.Click += new System.EventHandler(this.Button20_Click);
            // 
            // Pi
            // 
            this.Pi.BackColor = System.Drawing.Color.DarkRed;
            this.Pi.FlatAppearance.BorderSize = 0;
            this.Pi.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Coral;
            this.Pi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Pi.Location = new System.Drawing.Point(0, 0);
            this.Pi.Name = "Pi";
            this.Pi.Size = new System.Drawing.Size(50, 50);
            this.Pi.TabIndex = 22;
            this.Pi.Text = "Pi";
            this.Pi.UseVisualStyleBackColor = false;
            this.Pi.Click += new System.EventHandler(this.Button21_Click);
            // 
            // log
            // 
            this.log.BackColor = System.Drawing.Color.DarkRed;
            this.log.FlatAppearance.BorderSize = 0;
            this.log.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Coral;
            this.log.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.log.Location = new System.Drawing.Point(50, 0);
            this.log.Name = "log";
            this.log.Size = new System.Drawing.Size(50, 50);
            this.log.TabIndex = 23;
            this.log.Text = "log";
            this.log.UseVisualStyleBackColor = false;
            this.log.Click += new System.EventHandler(this.Button22_Click);
            // 
            // sqrt
            // 
            this.sqrt.BackColor = System.Drawing.Color.DarkRed;
            this.sqrt.FlatAppearance.BorderSize = 0;
            this.sqrt.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Coral;
            this.sqrt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sqrt.Location = new System.Drawing.Point(100, 0);
            this.sqrt.Name = "sqrt";
            this.sqrt.Size = new System.Drawing.Size(50, 50);
            this.sqrt.TabIndex = 24;
            this.sqrt.Text = "√";
            this.sqrt.UseVisualStyleBackColor = false;
            this.sqrt.Click += new System.EventHandler(this.Button23_Click);
            // 
            // x2
            // 
            this.x2.BackColor = System.Drawing.Color.DarkRed;
            this.x2.FlatAppearance.BorderSize = 0;
            this.x2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Coral;
            this.x2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.x2.Location = new System.Drawing.Point(150, 0);
            this.x2.Name = "x2";
            this.x2.Size = new System.Drawing.Size(50, 50);
            this.x2.TabIndex = 25;
            this.x2.Text = "x^2";
            this.x2.UseVisualStyleBackColor = false;
            this.x2.Click += new System.EventHandler(this.Button24_Click);
            // 
            // x3
            // 
            this.x3.FlatAppearance.BorderSize = 0;
            this.x3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Coral;
            this.x3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.x3.Location = new System.Drawing.Point(150, 50);
            this.x3.Name = "x3";
            this.x3.Size = new System.Drawing.Size(50, 50);
            this.x3.TabIndex = 26;
            this.x3.Text = "x^3";
            this.x3.UseVisualStyleBackColor = true;
            this.x3.Click += new System.EventHandler(this.Button25_Click);
            // 
            // dec
            // 
            this.dec.FlatAppearance.BorderSize = 0;
            this.dec.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Coral;
            this.dec.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dec.Location = new System.Drawing.Point(100, 50);
            this.dec.Name = "dec";
            this.dec.Size = new System.Drawing.Size(50, 50);
            this.dec.TabIndex = 27;
            this.dec.Text = "dec";
            this.dec.UseVisualStyleBackColor = true;
            this.dec.Click += new System.EventHandler(this.Button26_Click);
            // 
            // sin
            // 
            this.sin.FlatAppearance.BorderSize = 0;
            this.sin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Coral;
            this.sin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sin.Location = new System.Drawing.Point(50, 50);
            this.sin.Name = "sin";
            this.sin.Size = new System.Drawing.Size(50, 50);
            this.sin.TabIndex = 28;
            this.sin.Text = "sin";
            this.sin.UseVisualStyleBackColor = true;
            this.sin.Click += new System.EventHandler(this.Button27_Click);
            // 
            // sinh
            // 
            this.sinh.FlatAppearance.BorderSize = 0;
            this.sinh.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Coral;
            this.sinh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sinh.Location = new System.Drawing.Point(0, 50);
            this.sinh.Name = "sinh";
            this.sinh.Size = new System.Drawing.Size(50, 50);
            this.sinh.TabIndex = 29;
            this.sinh.Text = "sinh";
            this.sinh.UseVisualStyleBackColor = true;
            this.sinh.Click += new System.EventHandler(this.Button28_Click);
            // 
            // x1
            // 
            this.x1.FlatAppearance.BorderSize = 0;
            this.x1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Coral;
            this.x1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.x1.Location = new System.Drawing.Point(150, 100);
            this.x1.Name = "x1";
            this.x1.Size = new System.Drawing.Size(50, 50);
            this.x1.TabIndex = 33;
            this.x1.Text = "1/x";
            this.x1.UseVisualStyleBackColor = true;
            this.x1.Click += new System.EventHandler(this.Button29_Click);
            // 
            // bin
            // 
            this.bin.FlatAppearance.BorderSize = 0;
            this.bin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Coral;
            this.bin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bin.Location = new System.Drawing.Point(100, 100);
            this.bin.Name = "bin";
            this.bin.Size = new System.Drawing.Size(50, 50);
            this.bin.TabIndex = 32;
            this.bin.Text = "bin";
            this.bin.UseVisualStyleBackColor = true;
            this.bin.Click += new System.EventHandler(this.Button30_Click);
            // 
            // cos
            // 
            this.cos.FlatAppearance.BorderSize = 0;
            this.cos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Coral;
            this.cos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cos.Location = new System.Drawing.Point(50, 100);
            this.cos.Name = "cos";
            this.cos.Size = new System.Drawing.Size(50, 50);
            this.cos.TabIndex = 31;
            this.cos.Text = "cos";
            this.cos.UseVisualStyleBackColor = true;
            this.cos.Click += new System.EventHandler(this.Button31_Click);
            // 
            // cosh
            // 
            this.cosh.FlatAppearance.BorderSize = 0;
            this.cosh.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Coral;
            this.cosh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cosh.Location = new System.Drawing.Point(0, 100);
            this.cosh.Name = "cosh";
            this.cosh.Size = new System.Drawing.Size(50, 50);
            this.cosh.TabIndex = 30;
            this.cosh.Text = "cosh";
            this.cosh.UseVisualStyleBackColor = true;
            this.cosh.Click += new System.EventHandler(this.Button32_Click);
            // 
            // lnx
            // 
            this.lnx.FlatAppearance.BorderSize = 0;
            this.lnx.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Coral;
            this.lnx.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lnx.Location = new System.Drawing.Point(150, 150);
            this.lnx.Name = "lnx";
            this.lnx.Size = new System.Drawing.Size(50, 50);
            this.lnx.TabIndex = 37;
            this.lnx.Text = "ln(x)";
            this.lnx.UseVisualStyleBackColor = true;
            this.lnx.Click += new System.EventHandler(this.Button33_Click);
            // 
            // hex
            // 
            this.hex.FlatAppearance.BorderSize = 0;
            this.hex.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Coral;
            this.hex.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.hex.Location = new System.Drawing.Point(100, 150);
            this.hex.Name = "hex";
            this.hex.Size = new System.Drawing.Size(50, 50);
            this.hex.TabIndex = 36;
            this.hex.Text = "hex";
            this.hex.UseVisualStyleBackColor = true;
            this.hex.Click += new System.EventHandler(this.Button34_Click);
            // 
            // tan
            // 
            this.tan.FlatAppearance.BorderSize = 0;
            this.tan.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Coral;
            this.tan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tan.Location = new System.Drawing.Point(50, 150);
            this.tan.Name = "tan";
            this.tan.Size = new System.Drawing.Size(50, 50);
            this.tan.TabIndex = 35;
            this.tan.Text = "tan";
            this.tan.UseVisualStyleBackColor = true;
            this.tan.Click += new System.EventHandler(this.Button35_Click);
            // 
            // tanh
            // 
            this.tanh.FlatAppearance.BorderSize = 0;
            this.tanh.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Coral;
            this.tanh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tanh.Location = new System.Drawing.Point(0, 150);
            this.tanh.Name = "tanh";
            this.tanh.Size = new System.Drawing.Size(50, 50);
            this.tanh.TabIndex = 34;
            this.tanh.Text = "tanh";
            this.tanh.UseVisualStyleBackColor = true;
            this.tanh.Click += new System.EventHandler(this.Button36_Click);
            // 
            // pros
            // 
            this.pros.FlatAppearance.BorderSize = 0;
            this.pros.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Coral;
            this.pros.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pros.Location = new System.Drawing.Point(150, 200);
            this.pros.Name = "pros";
            this.pros.Size = new System.Drawing.Size(50, 50);
            this.pros.TabIndex = 41;
            this.pros.Text = "%";
            this.pros.UseVisualStyleBackColor = true;
            this.pros.Click += new System.EventHandler(this.Button37_Click);
            // 
            // oct
            // 
            this.oct.FlatAppearance.BorderSize = 0;
            this.oct.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Coral;
            this.oct.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.oct.Location = new System.Drawing.Point(100, 200);
            this.oct.Name = "oct";
            this.oct.Size = new System.Drawing.Size(50, 50);
            this.oct.TabIndex = 40;
            this.oct.Text = "oct";
            this.oct.UseVisualStyleBackColor = true;
            this.oct.Click += new System.EventHandler(this.Button38_Click);
            // 
            // mod
            // 
            this.mod.FlatAppearance.BorderSize = 0;
            this.mod.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Coral;
            this.mod.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.mod.Location = new System.Drawing.Point(50, 200);
            this.mod.Name = "mod";
            this.mod.Size = new System.Drawing.Size(50, 50);
            this.mod.TabIndex = 39;
            this.mod.Text = "mod";
            this.mod.UseVisualStyleBackColor = true;
            // 
            // exp
            // 
            this.exp.FlatAppearance.BorderSize = 0;
            this.exp.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Coral;
            this.exp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.exp.Location = new System.Drawing.Point(0, 200);
            this.exp.Name = "exp";
            this.exp.Size = new System.Drawing.Size(50, 50);
            this.exp.TabIndex = 38;
            this.exp.Text = "exp";
            this.exp.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 24);
            this.label1.TabIndex = 42;
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.Color.White;
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox3.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.textBox3.Location = new System.Drawing.Point(7, 199);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(248, 25);
            this.textBox3.TabIndex = 9;
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.White;
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox2.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox2.Location = new System.Drawing.Point(7, 65);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(248, 25);
            this.textBox2.TabIndex = 8;
            // 
            // button42
            // 
            this.button42.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(17)))), ((int)(((byte)(35)))));
            this.button42.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.button42.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.button42.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button42.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button42.Location = new System.Drawing.Point(142, 229);
            this.button42.Name = "button42";
            this.button42.Size = new System.Drawing.Size(113, 28);
            this.button42.TabIndex = 7;
            this.button42.Text = "Очистить";
            this.button42.UseVisualStyleBackColor = false;
            this.button42.Click += new System.EventHandler(this.Button42_Click);
            // 
            // button41
            // 
            this.button41.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(17)))), ((int)(((byte)(35)))));
            this.button41.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.button41.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button41.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button41.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button41.Location = new System.Drawing.Point(8, 229);
            this.button41.Name = "button41";
            this.button41.Size = new System.Drawing.Size(113, 28);
            this.button41.TabIndex = 6;
            this.button41.Text = "Конвертировать";
            this.button41.UseVisualStyleBackColor = false;
            this.button41.Click += new System.EventHandler(this.Button41_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(3, 167);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 29);
            this.label3.TabIndex = 5;
            this.label3.Text = "Результат";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(3, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(254, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "Введите значение для преобразования";
            // 
            // Fahrenheit_To_Celsius
            // 
            this.Fahrenheit_To_Celsius.AutoSize = true;
            this.Fahrenheit_To_Celsius.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Fahrenheit_To_Celsius.Location = new System.Drawing.Point(7, 119);
            this.Fahrenheit_To_Celsius.Name = "Fahrenheit_To_Celsius";
            this.Fahrenheit_To_Celsius.Size = new System.Drawing.Size(248, 20);
            this.Fahrenheit_To_Celsius.TabIndex = 3;
            this.Fahrenheit_To_Celsius.Text = "Градусы Фаренгейта в градусы Цельсия";
            this.Fahrenheit_To_Celsius.UseVisualStyleBackColor = true;
            this.Fahrenheit_To_Celsius.CheckedChanged += new System.EventHandler(this.Fahrenheit_To_Celsius_CheckedChanged);
            // 
            // Вegrees_Kevin
            // 
            this.Вegrees_Kevin.AutoSize = true;
            this.Вegrees_Kevin.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Вegrees_Kevin.Location = new System.Drawing.Point(7, 144);
            this.Вegrees_Kevin.Name = "Вegrees_Kevin";
            this.Вegrees_Kevin.Size = new System.Drawing.Size(130, 20);
            this.Вegrees_Kevin.TabIndex = 2;
            this.Вegrees_Kevin.Text = "Градусы Кельвина";
            this.Вegrees_Kevin.UseVisualStyleBackColor = true;
            this.Вegrees_Kevin.CheckedChanged += new System.EventHandler(this.Вegrees_Kevin_CheckedChanged);
            // 
            // Celsius_To_Fahrenheit
            // 
            this.Celsius_To_Fahrenheit.AutoSize = true;
            this.Celsius_To_Fahrenheit.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Celsius_To_Fahrenheit.Location = new System.Drawing.Point(7, 94);
            this.Celsius_To_Fahrenheit.Name = "Celsius_To_Fahrenheit";
            this.Celsius_To_Fahrenheit.Size = new System.Drawing.Size(248, 20);
            this.Celsius_To_Fahrenheit.TabIndex = 0;
            this.Celsius_To_Fahrenheit.TabStop = true;
            this.Celsius_To_Fahrenheit.Text = "Градусы Цельсия в градусы Фаренгейта";
            this.Celsius_To_Fahrenheit.UseVisualStyleBackColor = true;
            this.Celsius_To_Fahrenheit.CheckedChanged += new System.EventHandler(this.Celsius_To_Fahrenheit_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial Narrow", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(3, 235);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(138, 22);
            this.label4.TabIndex = 10;
            this.label4.Text = "Введите значение";
            // 
            // textBox4
            // 
            this.textBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox4.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.textBox4.Location = new System.Drawing.Point(142, 233);
            this.textBox4.Multiline = true;
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(113, 25);
            this.textBox4.TabIndex = 3;
            // 
            // button44
            // 
            this.button44.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(17)))), ((int)(((byte)(35)))));
            this.button44.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.button44.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.button44.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button44.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold);
            this.button44.Location = new System.Drawing.Point(142, 269);
            this.button44.Name = "button44";
            this.button44.Size = new System.Drawing.Size(113, 28);
            this.button44.TabIndex = 2;
            this.button44.Text = "Очистить";
            this.button44.UseVisualStyleBackColor = false;
            this.button44.Click += new System.EventHandler(this.Button44_Click);
            // 
            // button43
            // 
            this.button43.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(17)))), ((int)(((byte)(35)))));
            this.button43.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.button43.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button43.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button43.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold);
            this.button43.Location = new System.Drawing.Point(8, 269);
            this.button43.Name = "button43";
            this.button43.Size = new System.Drawing.Size(113, 28);
            this.button43.TabIndex = 1;
            this.button43.Text = "Умножить";
            this.button43.UseVisualStyleBackColor = false;
            this.button43.Click += new System.EventHandler(this.Button43_Click);
            // 
            // listBox1
            // 
            this.listBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listBox1.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 20;
            this.listBox1.Location = new System.Drawing.Point(7, 35);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(248, 182);
            this.listBox1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Red;
            this.panel1.Controls.Add(this.Pi);
            this.panel1.Controls.Add(this.log);
            this.panel1.Controls.Add(this.sqrt);
            this.panel1.Controls.Add(this.x2);
            this.panel1.Controls.Add(this.pros);
            this.panel1.Controls.Add(this.x3);
            this.panel1.Controls.Add(this.oct);
            this.panel1.Controls.Add(this.dec);
            this.panel1.Controls.Add(this.mod);
            this.panel1.Controls.Add(this.sin);
            this.panel1.Controls.Add(this.exp);
            this.panel1.Controls.Add(this.sinh);
            this.panel1.Controls.Add(this.lnx);
            this.panel1.Controls.Add(this.cosh);
            this.panel1.Controls.Add(this.hex);
            this.panel1.Controls.Add(this.cos);
            this.panel1.Controls.Add(this.tan);
            this.panel1.Controls.Add(this.bin);
            this.panel1.Controls.Add(this.tanh);
            this.panel1.Controls.Add(this.x1);
            this.panel1.Location = new System.Drawing.Point(200, 83);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 250);
            this.panel1.TabIndex = 45;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Red;
            this.panel2.Controls.Add(this.delete);
            this.panel2.Controls.Add(this.clear);
            this.panel2.Controls.Add(this.clear_all);
            this.panel2.Controls.Add(this.plus);
            this.panel2.Controls.Add(this.seven);
            this.panel2.Controls.Add(this.equals);
            this.panel2.Controls.Add(this.eight);
            this.panel2.Controls.Add(this.comma);
            this.panel2.Controls.Add(this.nine);
            this.panel2.Controls.Add(this.zero);
            this.panel2.Controls.Add(this.minus);
            this.panel2.Controls.Add(this.minus_plus);
            this.panel2.Controls.Add(this.four);
            this.panel2.Controls.Add(this.division);
            this.panel2.Controls.Add(this.five);
            this.panel2.Controls.Add(this.three);
            this.panel2.Controls.Add(this.six);
            this.panel2.Controls.Add(this.two);
            this.panel2.Controls.Add(this.multiplication);
            this.panel2.Controls.Add(this.one);
            this.panel2.Location = new System.Drawing.Point(0, 83);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 250);
            this.panel2.TabIndex = 46;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Red;
            this.panel3.Controls.Add(this.textBox3);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.button41);
            this.panel3.Controls.Add(this.button42);
            this.panel3.Controls.Add(this.Fahrenheit_To_Celsius);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.textBox2);
            this.panel3.Controls.Add(this.Вegrees_Kevin);
            this.panel3.Controls.Add(this.Celsius_To_Fahrenheit);
            this.panel3.Location = new System.Drawing.Point(406, 27);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(260, 260);
            this.panel3.TabIndex = 47;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial Narrow", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(204, 42);
            this.label5.TabIndex = 10;
            this.label5.Text = "Температура";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Red;
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.textBox4);
            this.panel4.Controls.Add(this.listBox1);
            this.panel4.Controls.Add(this.button44);
            this.panel4.Controls.Add(this.button43);
            this.panel4.Location = new System.Drawing.Point(672, 27);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(260, 300);
            this.panel4.TabIndex = 48;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label6.Font = new System.Drawing.Font("Arial Narrow", 23.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(158, 36);
            this.label6.TabIndex = 11;
            this.label6.Text = "Умножение";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(17)))), ((int)(((byte)(35)))));
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox1.Location = new System.Drawing.Point(0, 53);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(200, 24);
            this.textBox1.TabIndex = 49;
            this.textBox1.Text = "0";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(17)))), ((int)(((byte)(35)))));
            this.ClientSize = new System.Drawing.Size(937, 347);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Калькулятор";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Normal_Calculator;
        private System.Windows.Forms.ToolStripMenuItem Engineering_Calculator;
        private System.Windows.Forms.ToolStripMenuItem Temperature;
        private System.Windows.Forms.ToolStripMenuItem Multiplication1;
        private System.Windows.Forms.Button delete;
        private System.Windows.Forms.Button clear;
        private System.Windows.Forms.Button clear_all;
        private System.Windows.Forms.Button plus;
        private System.Windows.Forms.Button minus;
        private System.Windows.Forms.Button nine;
        private System.Windows.Forms.Button eight;
        private System.Windows.Forms.Button seven;
        private System.Windows.Forms.Button multiplication;
        private System.Windows.Forms.Button six;
        private System.Windows.Forms.Button five;
        private System.Windows.Forms.Button four;
        private System.Windows.Forms.Button division;
        private System.Windows.Forms.Button three;
        private System.Windows.Forms.Button two;
        private System.Windows.Forms.Button one;
        private System.Windows.Forms.Button equals;
        private System.Windows.Forms.Button comma;
        private System.Windows.Forms.Button zero;
        private System.Windows.Forms.Button minus_plus;
        private System.Windows.Forms.Button Pi;
        private System.Windows.Forms.Button log;
        private System.Windows.Forms.Button sqrt;
        private System.Windows.Forms.Button x2;
        private System.Windows.Forms.Button x3;
        private System.Windows.Forms.Button dec;
        private System.Windows.Forms.Button sin;
        private System.Windows.Forms.Button sinh;
        private System.Windows.Forms.Button x1;
        private System.Windows.Forms.Button bin;
        private System.Windows.Forms.Button cos;
        private System.Windows.Forms.Button cosh;
        private System.Windows.Forms.Button lnx;
        private System.Windows.Forms.Button hex;
        private System.Windows.Forms.Button tan;
        private System.Windows.Forms.Button tanh;
        private System.Windows.Forms.Button pros;
        private System.Windows.Forms.Button oct;
        private System.Windows.Forms.Button mod;
        private System.Windows.Forms.Button exp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton Вegrees_Kevin;
        private System.Windows.Forms.RadioButton Celsius_To_Fahrenheit;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button42;
        private System.Windows.Forms.Button button41;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton Fahrenheit_To_Celsius;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Button button44;
        private System.Windows.Forms.Button button43;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ToolStripMenuItem Exit;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox1;
    }
}

