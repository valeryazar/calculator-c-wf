﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fourth_Version_Of_The_Calculator
{
    public partial class Form1 : Form
    {
        double Result = 0;
        string Operation = "";
        bool Enter_Value = false;

        float Celsius;
        float Fahrenheit; 
        float Kevin;
        char Operation_For_Temperature;

        public Form1()
        {
            InitializeComponent();
            
        }

        private void ОбычныйToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Width = 216;
            this.Height = 372;

            textBox1.Width = 200;
            textBox1.Height = 24;
            textBox1.Location = new Point(0, 53);

            menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(17)))), ((int)(((byte)(35)))));
            label1.Visible = true;
            textBox1.Visible = true;
            panel1.Visible = false;
            panel2.Visible = true;
            panel3.Visible = false;
            panel4.Visible = false;
        }

        private void ИнToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Width = 416;
            this.Height = 372;
            textBox1.Width = 400;
            textBox1.Height = 24;

            textBox1.Width = 400;
            textBox1.Height = 24;
            textBox1.Location = new Point(0, 53);

            menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(17)))), ((int)(((byte)(35)))));
            label1.Visible = true;
            textBox1.Visible = true;
            panel1.Visible = true;
            panel2.Visible = true;
            panel3.Visible = false;
            panel4.Visible = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Width = 216;
            this.Height = 372;

            //textBox1.Width = 200; // удалить 
            //textBox1.Height = 24; // удалить 
            //textBox1.Location = new Point(0, 53); // удалить 

            menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(17)))), ((int)(((byte)(35)))));
            label1.Visible = true;
            textBox1.Visible = true;
            panel1.Visible = false;
            panel2.Visible = true;
            panel3.Visible = false;
            panel4.Visible = false;
        }

        private void ТемператураToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Width = 276;
            this.Height = 323;
            menuStrip1.BackColor = System.Drawing.Color.Red;
            //textBox2.Focus();
            label1.Visible = false;
            textBox1.Visible = false;
            panel1.Visible = false;
            panel2.Visible = false;
            panel3.Visible = true;
            panel4.Visible = false;
            panel3.Location = new Point(0, 24);
            panel3.Width = 260;
            panel3.Height = 260;
        }

        /*private void ЕдиницаИзмеренияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Width = 800;
            textBox1.Width = 218;
        }*/

        private void ТаблицаУмноженияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Width = 276;
            this.Height = 363;
            menuStrip1.BackColor = System.Drawing.Color.Red;
            //textBox4.Focus();
            label1.Visible = false;
            textBox1.Visible = false;
            panel1.Visible = false;
            panel2.Visible = false;
            panel3.Visible = false;
            panel4.Visible = true;
            panel4.Location = new Point(0, 24);
            panel4.Width = 260;
            panel4.Height = 300;
        }

        private void Button_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "0" || (Enter_Value))
            {
                textBox1.Text = "";
            }
            Enter_Value = false;
            Button B = (Button)sender;
            if (B.Text == ".")
            {
                if(!textBox1.Text.Contains("."))
                {
                    textBox1.Text += B.Text;
                }
            }
            else
            {
                textBox1.Text += B.Text;
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = "0";
            label1.Text = "";
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            textBox1.Text = "0";
            label1.Text = "";
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length > 0)
            {
                textBox1.Text = textBox1.Text.Remove(textBox1.Text.Length - 1, 1); ;
            }
            if (textBox1.Text == "")
            {
                textBox1.Text = "0";
            }
        }

        private void Button_Aritmetic(object sender, EventArgs e)
        {
            Button B = (Button)sender;
            Operation = B.Text;
            Result = Double.Parse(textBox1.Text);
            textBox1.Text = "";
            label1.Text = System.Convert.ToString(Result) + " " + Operation;
        }

        private void Button17_Click(object sender, EventArgs e)
        {
            label1.Text = "";
            switch (Operation)
            {
                case "+":
                    textBox1.Text = (Result + Double.Parse(textBox1.Text)).ToString();
                    break;
                case "-":
                    textBox1.Text = (Result - Double.Parse(textBox1.Text)).ToString();
                    break;
                case "*":
                    textBox1.Text = (Result * Double.Parse(textBox1.Text)).ToString();
                    break;
                case "/":
                    if (textBox1.Text == "0")
                    {
                        textBox1.Text = "Деление на ноль невозможно.";
                        break;
                    }
                    textBox1.Text = (Result / Double.Parse(textBox1.Text)).ToString();
                    break;
                case "mod":
                    textBox1.Text = (Result % Double.Parse(textBox1.Text)).ToString();
                    break;
                case "exp":
                    double i = Double.Parse(textBox1.Text);
                    double q;
                    q = (Result);
                    textBox1.Text = Math.Exp(i * Math.Log(q * 4)).ToString();
                    break;
            }
        }

        private void Button21_Click(object sender, EventArgs e)
        {
            textBox1.Text = "3.14159265359";
        }

        private void Button22_Click(object sender, EventArgs e)
        {
            Double Log10 = Double.Parse(textBox1.Text);
            label1.Text = System.Convert.ToString("log" + "(" + (textBox1.Text) + ")");
            Log10 = Math.Log10(Log10);
            textBox1.Text = System.Convert.ToString(Log10);
        }

        private void Button23_Click(object sender, EventArgs e)
        {
            Double Sqrt = Double.Parse(textBox1.Text);
            label1.Text = System.Convert.ToString("√" + (textBox1.Text));
            Sqrt = Math.Sqrt(Sqrt);
            textBox1.Text = System.Convert.ToString(Sqrt);
        }

        private void Button28_Click(object sender, EventArgs e)
        {
            Double SinH = Double.Parse(textBox1.Text);
            label1.Text = System.Convert.ToString("sinh" + "(" + (textBox1.Text) + ")");
            SinH = Math.Sinh(SinH);
            textBox1.Text = System.Convert.ToString(SinH);
        }

        private void Button27_Click(object sender, EventArgs e)
        {
            Double Sin = Double.Parse(textBox1.Text);
            label1.Text = System.Convert.ToString("sin" + "(" + (textBox1.Text) + ")");
            Sin = Math.Sin(Sin);
            textBox1.Text = System.Convert.ToString(Sin);
        }

        private void Button32_Click(object sender, EventArgs e)
        {
            Double CosH = Double.Parse(textBox1.Text);
            label1.Text = System.Convert.ToString("cosh" + "(" + (textBox1.Text) + ")");
            CosH = Math.Cosh(CosH);
            textBox1.Text = System.Convert.ToString(CosH);
        }

        private void Button31_Click(object sender, EventArgs e)
        {
            Double Cos = Double.Parse(textBox1.Text);
            label1.Text = System.Convert.ToString("cos" + "(" + (textBox1.Text) + ")");
            Cos = Math.Cos(Cos);
            textBox1.Text = System.Convert.ToString(Cos);
        }

        private void Button36_Click(object sender, EventArgs e)
        {
            Double TanH = Double.Parse(textBox1.Text);
            label1.Text = System.Convert.ToString("tanh" + "(" + (textBox1.Text) + ")");
            TanH = Math.Tanh(TanH);
            textBox1.Text = System.Convert.ToString(TanH);
        }

        private void Button35_Click(object sender, EventArgs e)
        {
            Double Tan = Double.Parse(textBox1.Text);
            label1.Text = System.Convert.ToString("tan" + "(" + (textBox1.Text) + ")");
            Tan = Math.Tan(Tan);
            textBox1.Text = System.Convert.ToString(Tan);
        }

        private void Button30_Click(object sender, EventArgs e)
        {
            int A = int.Parse(textBox1.Text);
            textBox1.Text = System.Convert.ToString(A, 2);
        }

        private void Button34_Click(object sender, EventArgs e)
        {
            int A = int.Parse(textBox1.Text);
            textBox1.Text = System.Convert.ToString(A, 16);
        }

        private void Button38_Click(object sender, EventArgs e)
        {
            int A = int.Parse(textBox1.Text);
            textBox1.Text = System.Convert.ToString(A, 8);
        }

        private void Button26_Click(object sender, EventArgs e)
        {
            int A = int.Parse(textBox1.Text);
            textBox1.Text = System.Convert.ToString(A);
        }

        private void Button24_Click(object sender, EventArgs e)
        {
            double A;
            label1.Text = System.Convert.ToString("sqr" + "(" + (textBox1.Text) + ")");
            A = Convert.ToDouble(textBox1.Text) * Convert.ToDouble(textBox1.Text);
            textBox1.Text = System.Convert.ToString(A);
        }

        private void Button25_Click(object sender, EventArgs e)
        {
            double A;
            label1.Text = System.Convert.ToString("cube" + "(" + (textBox1.Text) + ")");
            A = Convert.ToDouble(textBox1.Text) * Convert.ToDouble(textBox1.Text) * Convert.ToDouble(textBox1.Text);
            textBox1.Text = System.Convert.ToString(A);
        }

        private void Button29_Click(object sender, EventArgs e)
        {
            double A;
            label1.Text = System.Convert.ToString("reciproc" + "(" + (textBox1.Text) + ")");
            A = Convert.ToDouble(1.0) / Convert.ToDouble(textBox1.Text);
            textBox1.Text = System.Convert.ToString(A);
        }

        private void Button33_Click(object sender, EventArgs e)
        {
            Double Log = Double.Parse(textBox1.Text);
            label1.Text = System.Convert.ToString("ln" + "(" + (textBox1.Text) + ")");
            Log = Math.Log(Log);
            textBox1.Text = System.Convert.ToString(Log);
        }

        private void Button37_Click(object sender, EventArgs e)
        {
            double A;
            A = Convert.ToDouble(textBox1.Text) / Convert.ToDouble(100);
            textBox1.Text = System.Convert.ToString(A);
        }

        private void Button41_Click(object sender, EventArgs e)
        {
            switch(Operation_For_Temperature)
            {
                case 'C':
                    // Градусы Цельсия в градусы Фаренгейта
                    Celsius = float.Parse(textBox2.Text);
                    textBox3.Text = ((((9 * Celsius) / 5) + 32).ToString());
                    break;
                case 'F':
                    // Градусы Фаренгейта в градусы Цельсия
                    Fahrenheit = float.Parse(textBox2.Text);
                    textBox3.Text = ((((Fahrenheit - 32) * 5) / 9).ToString());
                    break;
                case 'K':
                    // Градусы Кельвина
                    Kevin = float.Parse(textBox2.Text);
                    textBox3.Text = (((((9 * Kevin) / 5) + 32) + 273.15).ToString());
                    break;

            }
        }

        private void Celsius_To_Fahrenheit_CheckedChanged(object sender, EventArgs e)
        {
            Operation_For_Temperature = 'C';
        }

        private void Fahrenheit_To_Celsius_CheckedChanged(object sender, EventArgs e)
        {
            Operation_For_Temperature = 'F';
        }

        private void Вegrees_Kevin_CheckedChanged(object sender, EventArgs e)
        {
            Operation_For_Temperature = 'K';
        }

        private void Button42_Click(object sender, EventArgs e)
        {
            textBox3.Clear();
            textBox3.Text = "";
            Celsius_To_Fahrenheit.Checked = false;
            Fahrenheit_To_Celsius.Checked = false;
            Вegrees_Kevin.Checked = false;
        }

        private void Button43_Click(object sender, EventArgs e)
        {
            int A;
            A = Convert.ToInt32(textBox4.Text);
            for (int i = 1; i < 10; i++)
            {
                listBox1.Items.Add(i + " x " + A + " = " + A * i);
            }
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Button44_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            textBox4.Clear();
        }

        private void Button20_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "0" || (Enter_Value))
            {
                textBox1.Text = "";
            }
            Enter_Value = false;
            if (textBox1.Text.Length > 0)
            {
                if (!textBox1.Text.Contains("-"))
                {
                    textBox1.Text = "-" + textBox1.Text;
                }
                else if (textBox1.Text.Contains("-"))
                {
                    textBox1.Text = textBox1.Text.Substring(1, textBox1.Text.Length - 1);
                }
            }
        }

        private void ExitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /*private void TextBox4_TextChanged(object sender, EventArgs e)
        {

        }*/

        /*private void Label5_Click(object sender, EventArgs e)
        {

        }*/

        /*private void TextBox3_TextChanged(object sender, EventArgs e)
        {

        }*/
    }
}
